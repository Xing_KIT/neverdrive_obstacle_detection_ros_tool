#include "obstacle_detection.hpp"

using namespace std;
using namespace cv;

namespace neverdrive_obstacle_detection_ros_tool {

ObstacleDetection::ObstacleDetection(ros::NodeHandle nhPublic, ros::NodeHandle nhPrivate)
        : interface_{nhPrivate}, reconfigureServer_{nhPrivate} {

    /**
     * Initialization
     */
    interface_.fromParamServer();

    /**
     * Set up callbacks for subscribers and reconfigure.
     *
     * New subscribers can be created with "add_subscriber" in "cfg/ObstacleDetection.if file.
     * Don't forget to register your callbacks here!
     */
    reconfigureServer_.setCallback(boost::bind(&ObstacleDetection::reconfigureRequest, this, _1, _2));

    //cv::namedWindow("window");

    ground_depth_ = cv::imread(interface_.path_to_ground_depth + "ground_depth.png", cv::IMREAD_ANYDEPTH);      //??????????????

    ROS_INFO_STREAM("ground depth size: "<<ground_depth_.rows<<" "<<ground_depth_.cols<<endl);
    depth_image_subscriber_ = nhPrivate.subscribe("/kinect2/qhd/image_depth_rect",
                                               1,
                                               &ObstacleDetection::depthImageCallback,
                                               this);

    depth_image_publisher_ = nhPrivate.advertise<sensor_msgs::Image>("/obstacle_detection_depth_image",1);
    //depth_image_pub_timer_ =

    rosinterface_handler::showNodeInfo();
}

void ObstacleDetection::depthImageCallback(const Msg::ConstPtr& msg) {

    cv_bridge::CvImageConstPtr cvPtrImage;
    cvPtrImage = cv_bridge::toCvShare(msg);       //defaule CV_16U?????
    cv::Mat depthImage;
    depthImage = cvPtrImage->image;

    maskDepth(ground_depth_, depthImage, depthImage, 300, 2000);

    depthImage.convertTo(depthImage,CV_8UC1,1.0/16); // convert the image into 8 bit, distance/16
    vector<vector<Point> > result;
    result = find_obstacles(depthImage, 20, 255, 5000);  // 20*16 = 320mm





    //convert opencv image to image msgs and publish it
    sensor_msgs::ImagePtr depthMsg;
    std_msgs::Header header;
    header.stamp = ros::Time::now();
    depthMsg = cv_bridge::CvImage(header,"mono16",depthImage).toImageMsg();
    //depth_image_publisher_.publish(depthMsg);

}


bool ObstacleDetection::pixelInMapArea(cv::Mat & depth,int v,int u){
    double z = (double)depth.at<uint16_t>(v, u) / 1000;

    pcl::PointXYZ point_in;
    point_in.x = z / interface_.ir_focal_length * (u - interface_.ir_u0);
    point_in.y = z / interface_.ir_focal_length * (v - interface_.ir_v0);
    point_in.z = z;

    pcl::PointXYZ out = pcl::transformPoint(point_in, ir_2_world_);

    if(out.x < interface_.map_area_max_x && out.x > interface_.map_area_min_x
       && out.y < interface_.map_area_max_y && out.y > interface_.map_area_min_y
       && out.z < interface_.map_area_max_z && out.z > interface_.map_area_min_z){
        return true;
    }

    return false;
}

void ObstacleDetection::getIr2World(){
    while(1) {
        try {
            const geometry_msgs::TransformStamped tf_ros =
                    tfBuffer_.lookupTransform(interface_.world_frame, interface_.kinect_ir_frame, ros::Time(0));
            ir_2_world_ = tf2::transformToEigen(tf_ros);
            break;
        } catch (const tf2::TransformException &e) {
            ROS_WARN_STREAM(e.what());
        }
    }

    ROS_INFO_STREAM("IR 2 WORLD"<< ir_2_world_.translation()<<endl);
}


void ObstacleDetection::maskDepth(cv::Mat & ground, cv::Mat &image, cv::Mat &img, int minThreshold , int maxThreshold ){

    getIr2World();
    int nr = image.rows;
    int nc = image.cols;
    for(int i=0;i<nr;i++){
        for(int j=0;j<nc;j++){
            if( image.at<ushort>(i,j) > minThreshold &&  image.at<ushort>(i,j) < maxThreshold){
                //img.at<ushort>(i,j) = 65535;
                if(abs(image.at<ushort>(i,j) - ground.at<ushort>(i,j)) < ground_threshold_){
                    img.at<ushort>(i,j)= 0;
                }
                else{
                    if(pixelInMapArea(image,i,j)) {
                        img.at<ushort>(i, j) = 65535;
                    }
                    else{
                        img.at<ushort>(i, j) = 0;
                    }
                }
            }
            else{
                img.at<ushort>(i,j)= 0;
            }
        }
    }
}

vector<vector<Point>> ObstacleDetection::find_obstacles(Mat &image, int minThre , int maxThre , int area ){
    Mat dep;
    image.copyTo(dep);
    //maskDepth(image,dep,55,65);
    //imshow("depth",dep);
    /// Open operator to avoid noise
    Mat element = getStructuringElement(MORPH_RECT,Size(15,15)); // choose the right size
    Mat out;
    morphologyEx(dep,out,MORPH_OPEN,element); // opening
    Mat src_copy = out.clone();
    Mat threshold_output;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    RNG rng(12345);
    /// binary
    threshold(out, threshold_output, minThre, maxThre, CV_THRESH_BINARY);
    /// find contours
    findContours(threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

    /// calculate hulls
    vector<vector<Point> >hull(contours.size());
    vector<vector<Point> > result;
    for (int i = 0; i < contours.size(); i++)  {
        convexHull(Mat(contours[i]), hull[i], false);
    }

    /// drwing the contours
    Mat drawing = Mat::zeros(threshold_output.size(), CV_8UC3);
    for (int i = 0; i< contours.size(); i++)  {
        if (contourArea(contours[i]) < area) // delete the hull small than 500
            continue;

        result.push_back(hull[i]);
        Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
        drawContours(drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point());
        Rect rect = boundingRect(contours[i]);
        cout<<"Rect info:"<<rect<<endl;
        cout<<"width(in pixel):"<<rect.width<<endl;
        cout<<"size of contours " << i <<" : "<<contours[i].size()<<endl;
        rectangle(out, rect, color, i,8, 0 );
        rectangle(drawing, rect, color, i,8, 0 );
        //rectangle(colorImage,rect,color,i,8,0);
        //imshow("Out",out);

    }
    //imshow("contours", drawing);

    sensor_msgs::ImagePtr depthMsg;
    std_msgs::Header header;
    header.stamp = ros::Time::now();
    depthMsg = cv_bridge::CvImage(header,"bgr8",drawing).toImageMsg();
    depth_image_publisher_.publish(depthMsg);
    return result;
}

/**
  * This callback is called at startup or whenever a change was made in the dynamic_reconfigure window
*/
void ObstacleDetection::reconfigureRequest(const Interface::Config& config, uint32_t level) {
    interface_.fromConfig(config);
}


} // namespace neverdrive_obstacle_detection_ros_tool
